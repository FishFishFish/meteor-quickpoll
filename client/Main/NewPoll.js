import {
    Meteor
} from 'meteor/meteor';
import {
    Polls
} from '/collections/polls.js';

Template.NewPoll.onCreated(function () {
    var self = this;
    self.autorun(function () {});
});
Template.NewPoll.onRendered(function () {
    var first = $("#0");
    options.push(first);
});

var options = [];
//TODO change options to int
var addImage = 0;

Template.NewPoll.events({
    'submit .new-poll-form' (event) {
        event.preventDefault();
        $('#question-text').html('');
        $('#options-text').html('');
        const target = event.target;
        const questionText = target.question.value;
        const image = target.image.value;
        var inputs = [];
        if(questionText != null && /\S/.test(questionText)){
            for(var i = 0; i < target.length; i++){
                if(Number(target[i].id) || target[i].id == '0'){
                    if(target[i].value != null && /\S/.test(target[i].value)){
                         inputs.push(target[i].value);
                    }
                }
            }
            if (inputs.length > 0){
                Meteor.call('polls.insert', questionText, inputs, image);
                options = [];
             }else{
                   $('<p>You need to fill in at least one option</p>').appendTo('#options-text');
             }
        }else{
             $('<p>Question cant be null</p>').appendTo('#question-text');
        }
    },
    'click .add-option' (event) {
        var length = options.length;
        var option = jQuery('<input/>', {
            id: length,
            name: length,
            type: 'text',
            autocomplete: "off",
            class: "option form-control",
            placeholder: "Enter option..."
        });
        option.appendTo('#options');
        options.push(option);
    },
    'click .add-image' (event){
        if(addImage == 0){
            addImage = 1;
            $('#image').show();
        }else{
            addImage = 0;
            $('#image').hide();        
        }
    }
});