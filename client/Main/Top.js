Template.Top.onCreated(function(){
    var self = this;
    self.autorun(function(){
        self.subscribe('PollsTop');
    });
});

Template.Top.helpers({
    Polls: ()=> {
        return Polls.find();
    }
});
Template.registerHelper('formatId', (id)=>(id && id._str || id))


