Template.Polls.onCreated(function(){
    var self = this;
    self.autorun(function(){
        self.subscribe('Polls');
    });
});

Template.Polls.helpers({
    Polls: ()=> {
        return Polls.find();
    }
});
Template.registerHelper('formatId', (id)=>(id && id._str || id))


