Template.Result.onCreated(function () {
    var self = this;
    self.autorun(function () {
        var id = FlowRouter.getParam('id');
        self.subscribe('singlePoll', id);
    });
});

//TODO if option is skipped on new poll bar wont be filled

var votes = []
Template.Result.helpers({
    Poll: () => {
        var id = FlowRouter.getParam('id');
        var poll = Polls.findOne({_id: id});
        for(var i = 0; i < poll.options.length; i++){
            var vote = (100/poll.totalVotes)*poll.options[i].votes;
            votes[i] = {vote: vote, id: poll.options[i].id};
        }
        return poll;
    },
    percentage(id){
        var val = $.grep(votes, function(e){ return e.id == id})
        return val[0].vote;
    }
});
