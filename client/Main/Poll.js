Template.Poll.onCreated(function () {
    var self = this;
    self.autorun(function () {
        var id = FlowRouter.getParam('id');
        self.subscribe('singlePoll', id);
        var cookie =document.cookie.indexOf(id); 
        if(cookie >= 0){
            FlowRouter.go('Result', {id:id});
        }
    });
});
//^ working

Template.Poll.helpers({
    Poll: () => {
        var id = FlowRouter.getParam('id');
        return Polls.findOne({_id: id});
    }
});


Template.Poll.events({
    'click .add-vote'(event){
        const target = event.target;
        var id = FlowRouter.getParam('id');
        //todo temp
        var socket_url = Meteor.default_connection._stream.socket._transport.url
        Meteor.call('polls.vote',id,target.id, socket_url)
        var d = new Date();
        d.setTime(d.getTime() + (365*24*60*60*1000));
        var expires = "expires="+ d.toUTCString();
        document.cookie = id + "=" + id + ";" + expires + ";path=/";
        FlowRouter.go('Result', {id: id})
    }
})