FlowRouter.route('/', {
    name: 'Home',
    action() {
        BlazeLayout.render('HomeLayout', {
            main: 'NewPoll'
        })
    }
});
FlowRouter.route('/Polls', {
    name: 'Polls',
    action() {
          BlazeLayout.render('HomeLayout', {
            main: 'Polls'
        })
    }
});
FlowRouter.route('/Top', {
    name: 'Top',
    action() {
          BlazeLayout.render('HomeLayout', {
            main: 'Top'
        })
    }
});
FlowRouter.route('/Poll/:id', {
    name: 'Poll',
    action() {
         BlazeLayout.render('HomeLayout', {
            main: 'Poll'
        })
    }
});
FlowRouter.route('/Poll/:id/result', {
    name: 'Result',
    action() {
         BlazeLayout.render('HomeLayout', {
            main: 'Result'
        })
    }
});