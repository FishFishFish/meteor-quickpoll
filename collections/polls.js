import {
    Mongo
} from 'meteor/mongo';

Polls = new Mongo.Collection('Polls');
Voters = new Mongo.Collection('Voters');

Meteor.methods({
    'polls.insert' (question, inputs,image) {
        check(question, String);
        var options = []
        var totalVotes = 0;
        for (i = 0; i < inputs.length; i++) {
            check(inputs[i], String)
            if (inputs[i])
                options.push({
                    id: i,
                    text: inputs[i],
                    votes: 0
                })
        }
        check(image, String);
        var id = Polls.insert({
            question,
            options,
            image,
            totalVotes,
            createdAt: new Date()
        })
        FlowRouter.go('Poll', {
            id: id
        });
    },
    'polls.vote' (id, optionId) {
        check(optionId, String);
        check(id, String);
        //TODO get 1 option only
        var voters = Voters.find({
            poll: id,
            ip: "127.0.0.1"
        });
        // if (voters <= 0)
        {
            var poll = Polls.findOne({_id: id})
            var options = poll.options;
            var totalvotes = poll.totalVotes;
            for (i = 0; i < options.length; i++) {
                if (options[i].id == optionId) {
                    options[i].votes += 1;
                }
            }
            totalvotes+=1;
            Voters.insert({ip: "127.0.0.1",poll: id});
            Polls.update(id, {$set: {options: options, totalVotes: totalvotes}})
        }

    }
});

// Polls.allow({
//     insert: function(){
//         return true;
//     }
// })