# QuickPoll #

### What is this repository for? ###

* Simple meteor app. It serves as a polling site. Allows the user to create and vote on polls.

![Poll.png](https://bitbucket.org/repo/Mj7a8z/images/4293628939-Poll.png)
![New empty.png](https://bitbucket.org/repo/Mj7a8z/images/1911718041-New%20empty.png)
![Top.png](https://bitbucket.org/repo/Mj7a8z/images/3370056521-Top.png)